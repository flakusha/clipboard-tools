from typing import Tuple

def check_os() -> Tuple[str, str]:
    '''Check OS type before execution and select the execution method'''

    return os.name, platform.system()

if __name__ == "__main__":
    
    import os
    import sys
    import platform
    
    sys.path.insert(0, os.path.abspath('..'))

    os_version = check_os()

    # TODO add modes
    
    if os_version[0] == 'nt':
        import win32clipboard
        from get_clipboard import get_clipboard_win

        while True:
            win32clipboard.OpenClipboard()
            clipboard = get_clipboard_win()
            win32clipboard.SetClipboardText(clipboard,\
            win32clipboard.CF_UNICODETEXT)
            win32clipboard.CloseClipboard()

    elif os_version[0] == 'posix':
        import subprocess
        from get_clipboard import get_clipboard_lin

        while True:
            # TODO add Linux support
            pass