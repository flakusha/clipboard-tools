def get_clipboard_win() -> str:
    '''Gets clipboard contents in Windows'''
    import win32clipboard
    import time
    from transliterator import transliterator_ru
    
    time.sleep(1)
    win32clipboard.OpenClipboard()
    
    try:
        clipboard = win32clipboard.GetClipboardData()
    except TypeError:
        print('Clipboard is empty')
        return ''
    
    print('Clipboard content:\n{}'.format(clipboard))
    
    clipboard_tr = transliterator_ru(clipboard)
    #print('Clipboard content after transliteration:\n{}'.format(clipboard_tr))
    
    if clipboard == clipboard_tr:
        return clipboard
    else:
        return clipboard_tr

'''
def get_clipboard_lin() -> str:
    Gets clipboard contents in Linux
    # TODO Linux execution and stuff
    return 'not implemented'
'''