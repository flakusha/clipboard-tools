def transliterator_ru(content) -> str:
    '''Transliterates to cyrillic using the assigned rules'''
    import re
    from transliterate import translit
    
    if bool(re.search('[\u0400-\u04FF]', content)):
        content_tr_ru = translit(content, 'ru', reversed = True)
    else:
        content_tr_ru = content
    #print('Clipboard content after transliteration:\n{}'.format(content_ru))
    
    # Characters to replace
    rep = (' ', '.', ',', '|', '\\', '/', ' ', ';', ':', ' ', '<', '>', '*',\
    "'", '"', '?', '-', '—')
    # Convert to string
    rep_conv = ''.join(rep)

    trans = str.maketrans(rep_conv, '_' * len(rep_conv))
    #print('Trans rule:\n{}'.format(trans))
    content_tr_ru = content_tr_ru.translate(trans)
    print('Replaced not compatible:\n{}'.format(content_tr_ru))

    underline_counter = 0

    content_tr_ru = re.sub('__+', '_', content_tr_ru)

    print('Final version of clipboard string:\n{}'.format(content_tr_ru))
    return content_tr_ru